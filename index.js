module.exports = {
    /**
     * @description
     * Suma de dos numeros enteros o decimales
     * @param {Number} n1 
     * @param {Number} n2 
     * @returns {Number} n1 + n2
     * @example 
     * n1 = 1
     * n2 = 2 
     * resultado(n1 + n2) = 3
     */
    suma: function (n1, n2) {
        return this.esNumero(n1, n2) ? n1 + n2 : this.mensajeError();
    },

    /**
     * @description
     * Resta de dos numeros enteros o decimales
     * @param {Number} n1 
     * @param {Number} n2 
     * @returns {Number} n1 - n2
     * @example 
     * n1 = 2
     * n2 = 1 
     * resultado(n1 - n2) = 1
     */
    resta: function (n1, n2) {
        return this.esNumero(n1, n2) ? n1 - n2 : this.mensajeError();
    },

    /**
     * @description
     * Multiplicación de dos numeros enteros o decimales
     * @param {Number} n1 
     * @param {Number} n2 
     * @returns {Number} n1 * n2
     * @example 
     * n1 = 2
     * n2 = 2 
     * resultado(n1 * n2) = 4
     */
    multiplicacion: function (n1, n2) {
        return this.esNumero(n1, n2) ? n1 * n2 : this.mensajeError();
    },

    /**
     * @description
     * División de dos numeros enteros o decimales
     * @param {Number} n1 
     * @param {Number} n2 
     * @returns {Number} n1 / n2
     * @example 
     * n1 = 4
     * n2 = 2 
     * resultado(n1 / n2) = 2
     */
    division: function (n1, n2) {
        return this.esNumero(n1, n2) ? n1 / n2 : this.mensajeError();
    },
    /**
     * @description
     * Muestra un mensaje de error.
     */
    mensajeError: function () {
        console.log('Un valor o los dos valores no son numericos');
    },
    /**
     * @description
     * Comprueba los valores de entrada, si estos son numericos.
     * @param {Number} n1 
     * @param {Number} n2 
     * @returns 
     */
    esNumero: function (n1, n2) {
        return typeof n1 !== 'number' || typeof n2 !== 'number' ? false : true;
    }
}