# Operaciones matematicas

Modulo para realizar las operaciones matematicas

## Instalacion
``` 
npm install proyecto-matematicas
```

# Uso
Seguir las instrucciones

```
npm install proyecto-matematicas

// Importamos el modulo

const m = require('.');

// Suma
console.log(m.suma(2, 2));

// Resta
console.log(m.resta(2, 2));

// Multiplicacion
console.log(m.multiplicacion(2, 2));

// Division
console.log(m.division(2, 2));
```